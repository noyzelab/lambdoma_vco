# Lambdoma_VCO

Simple Voltage Controlled Dual Lambdoma Oscillator for Arduino using Mozzi.
Implementation demonstrates a 2x sine oscillator implementation of Barbara Hero's Lambdoma Matrix Oscillator.
Oscillators are mixed and appear as audio out on pin 9.

This code requires MOZZI to run  => https://sensorium.github.io/Mozzi/

if u find this prog useful please think about supporting my work either thru my bandcamp page:
https://noyzelab.bandcamp.com/
or get in touch via noyzelab [at] gmail [dot] com
thanks, dave
