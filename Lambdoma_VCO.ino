// Simple Voltage Controlled Dual Lambdoma Oscillator for Arduino using Mozzi.
// Implementation demonstrates a basic 2x sine oscillator implementation of Barbara Hero's Lambdoma Matrix Oscillator.
// Each oscillator has independant row & column control voltage inputs.
// Independant fundamental and matrix size per oscillator.
// Oscillators are mixed and appear as audio out on pin 9.
// Compatible with uMANIAC preALPHA pinout [A0, A2, A3, A6], just change these to suit your needs / board
// Tested on Arduino Nano/Atmega 328.
// Dave Burraston 2020
// www.noyzelab.com

// This code requires MOZZI to run  =>
// https://sensorium.github.io/Mozzi/s

#include <MozziGuts.h>
#include <Oscil.h> 
#include <tables/sin2048_int8.h> 

Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin2(SIN2048_DATA);

#define CONTROL_RATE 64  

int row1, col1, row2, col2;
int matsize1, matsize2; // actual matrix size
int matdiv1, matdiv2; 
float fundamental1, fundamental2;
float freq1, freq2;

void setup(){
  matsize1 = 64;
  matsize2 = 64;
  matdiv1 = 1024/matsize1;
  matdiv2 = 1024/matsize2;

  fundamental1 = 33.0;
  fundamental2 = 66.0;
 
  row1 = 1+mozziAnalogRead(A0)/matdiv1;
  col1 = 1+mozziAnalogRead(A2)/matdiv1;
  row2 = 1+mozziAnalogRead(A3)/matdiv2;
  col2 = 1+mozziAnalogRead(A6)/matdiv2;
  
  freq1 = fundamental1 * (float(row1)/float(col1));
  freq2 = fundamental2 * (float(row2)/float(col2));
      
  startMozzi(CONTROL_RATE);  
  aSin.setFreq(freq1); 
  aSin2.setFreq(freq2); 
}

void updateControl(){

  row1 = 1+mozziAnalogRead(A0)/matdiv1;
  col1 = 1+mozziAnalogRead(A2)/matdiv1;
  row2 = 1+mozziAnalogRead(A3)/matdiv2;
  col2 = 1+mozziAnalogRead(A6)/matdiv2;

  freq1 = fundamental1 * (float(row1)/float(col1));   
  aSin.setFreq_Q24n8(float_to_Q24n8(freq1));
  freq2 = fundamental2 * (float(row2)/float(col2));
  aSin2.setFreq_Q24n8(float_to_Q24n8(freq2));
}

int updateAudio(){
   return aSin.next()/2 + aSin2.next()/2;  
}
 
void loop(){
  audioHook(); 
}